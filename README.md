# GITEA DRONE CI DOCKER-COMPOSE DEPLOYEMENT

Stack de déployement de Gitea avec Drone CI pour héberger son code avec des runners pour le CI/CD

[Gitea site](https://gitea.io)

[Drone CI site](https://www.drone.io)

## PRE-REQUIS

- [Docker](https://www.google.com)
- [Docker-compose](https://www.google.com)

## USAGE

Il est conseillé de déployer Gitea et Drone ci sur deux machines distinctes pour éviter d'éventuels conflits réseau.  

Déployer Gitea en premier, créer l'utilisateur administrateur et renseigner le nom de domaine
> Il faudrat deux sous domaines (dans cet exemple: git.mondomaine.tld et drone.mondomaine.tld, remplacer mondomaine.tld par votre nom de domaine et créer les configurations serveur adéquates)
Gitea est à servir sur le port 3000 et Drone ci sur le 3001 (modifiable dans le compose si besoin)

### GITEA

- Renseigner le fichier .env avec votre nom de domaine

- deployer gitea:
```bash
cd gitea/
docker-compose up -d
```

- Créer l'utilisateur admin avec son mot de passe

- Dans le profil de l'utilisateur, aller à l'onglet application pour créer l'authentification Oauth2 pour le drone (DRONE_GITEA_CLIENT_ID, DRONE_GITEA_CLIENT_SECRET) et renseigner celles-ci dans le fichier .env

- Renseigner le DRONE_RPC_SECRET (aéatoire, c'est pour authentifier le drone runner et le drone server entre eux)

> Pour utiliser le SSH, s'assurer que le port mappé est ouvert sur la machine hôte

### DRONE CI

- Renseigner le fichier .env avec le bon nom de domaine, le DRONE_RPC_SECRET (aéatoire, c'est pour authentifier le drone runner et le drone server entre eux) ainsi que le nom d'utilisateur et le mot de passe de l'admin créé plus haut


- deployer drone ci:
```bash
cd drone/
docker-compose up -d
```

## CONFIGURATION

- gitea/conf/app.ini
```
# DNS
[server]
DOMAIN           = gitea.mondomaine.tld
SSH_DOMAIN       = gitea.mondomaine.tld
ROOT_URL         = https://gitea.mondomaine.tld/
LANDING_PAGE     = explore

# SSO
[service]
DISABLE_REGISTRATION              = true
ALLOW_ONLY_EXTERNAL_REGISTRATION  = true
[openid]
ENABLE_OPENID_SIGNIN = true

# THEMES
[ui]
THEMES = gitea,arc-green,github,matrix,tangerine-dream,earl-grey
DEFAULT_THEME = gitea
```

> Pour le SSO, aller dans les la partie administration du site sur gitea, onglet source d'authentification et ajouter une source d'authentification (pour keycloak: https://adresse-du-keycloak/auth/realms/gregan/.well-known/openid-configuration)

## TO DO

- [ ] Config all via .env

## CONTRIBUTION

this docker-compose file is based on https://github.com/ruanbekker/drone-gitea-traefik-docker-blogpost